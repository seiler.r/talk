var script = document.createElement("script");
script.type = "module";
script.innerHTML = `
// import * as THREE from 'https://threejs.org/build/three.module.js';
// import { EffectComposer } from 'https://threejs.org/examples/jsm/postprocessing/EffectComposer.js';
// import { RenderPass } from 'https://threejs.org/examples/jsm/postprocessing/RenderPass.js';
// import { ShaderPass } from 'https://threejs.org/examples/jsm/postprocessing/ShaderPass.js';
// import { OutlinePass } from 'https://threejs.org/examples/jsm/postprocessing/OutlinePass.js';
// import {GLTFLoader} from 'https://threejs.org/examples/jsm/loaders/GLTFLoader.js';

import * as THREE from 'https://localhost:8080/build/three.module.js';
import { EffectComposer } from 'https://localhost:8080/postprocessing/EffectComposer.js';
import { RenderPass } from 'https://localhost:8080/postprocessing/RenderPass.js';
import { ShaderPass } from 'https://localhost:8080/postprocessing/ShaderPass.js';
import { OutlinePass } from 'https://localhost:8080/postprocessing/OutlinePass.js';
import {GLTFLoader} from 'https://localhost:8080/loaders/GLTFLoader.js';

let b = true;

//initialis htmlelements for event detection
var img = new Image();
var img2 = new Image();
let canvas = document.createElement("canvas");
var renderer;
let composer;

  var scene = new THREE.Scene();
  var camera = new THREE.PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    0.1,
    1000
  );

  var light = new THREE.DirectionalLight("#c1582d", 1);
  var ambient = new THREE.AmbientLight("#85b2cd");
  light.position.set(0, -70, 100).normalize();
  scene.add(light);
  scene.add(ambient);
  let height = 0;
  let rotation = 0;

  renderer = new THREE.WebGLRenderer({ alpha: true });
  renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(renderer.domElement);
  renderer.domElement.addEventListener("click",_ => height = Math.PI)

  composer = new EffectComposer(renderer);

  var renderPass = new RenderPass(scene, camera);
  composer.addPass(renderPass);

  let outlinePass = new OutlinePass(
    new THREE.Vector2(window.innerWidth, window.innerHeight),
    scene,
    camera
  );
  outlinePass.edgeThickness = 2;
  outlinePass.edgeStrength = 5;
  outlinePass.edgeGlow = 1;
  outlinePass.visibleEdgeColor.set( "#0000ff" );
  composer.addPass(outlinePass);

  //the event passthrough decision code
  //would tweak to request data in canvas instead of constantly in order to improve perf
  document.body.addEventListener("mousemove", e => {
    let d = canvas.getContext("2d").getImageData(e.clientX, e.clientY, 1, 1)
      .data;
    if (d[3]) {
      renderer.domElement.style.pointerEvents = "auto";
      outlinePass.selectedObjects = [earth]
    } else {
      renderer.domElement.style.pointerEvents = "none";
      outlinePass.selectedObjects = []
    }
  });

  camera.position.z = 75;

  //load the model
  var loader = new GLTFLoader();

  let earth;

  loader.load("https://localhost:8080/axolotl.glb", function(gltf) {
    earth = gltf.scene;
    earth.position.set(0, -40, 0);
    earth.scale.set(4, 4, 4);
    scene.add(earth);
  });

  //apply the style
  var style = document.createElement("style");
  style.innerHTML = "canvas {position:fixed;top:0}";
  document.head.appendChild(style);

  //render loop
  function animate() {
    requestAnimationFrame(animate);
    if (earth) {
      earth.rotation.y = Math.sin(rotation) / 4.0;
      earth.position.y = Math.sin(height) * 40 - 40;
      if(height > 0) {
        height -= 0.1;
      }
      rotation += 0.05;
    }
    // renderer.render(scene, camera);
    composer.render();

    canvasStuff();
  }
  animate();


function canvasStuff() {
  if (!b) {
    img.src = renderer.domElement.toDataURL("img/png");
    canvas.width = img2.width;
    canvas.height = img2.height;
    canvas.style.width = img2.width + "px";
    canvas.style.height = img2.height + "px";
    canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);
    canvas.getContext("2d").drawImage(img2, 0, 0);
  } else {
    img2.src = renderer.domElement.toDataURL("img/png");
    canvas.width = img.width;
    canvas.height = img.height;
    canvas.style.width = img.width + "px";
    canvas.style.height = img.height + "px";
    canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);
    canvas.getContext("2d").drawImage(img, 0, 0);
  }
  b = !b;
}

`;

document.head.appendChild(script);
